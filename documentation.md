---
layout: default
title: Documentation
---

## How to start the project:

### Get the project on Gitlab:
- Fork and clone from: [https://gitlab.com/Rikkeblom/epsilon_bank]( https://gitlab.com/Rikkeblom/epsilon_bank)

### Run the project from the folder 'epsilon_bank' containing 'docker-compose.yml' with either:
- RTE=test compose-up
- RTE=dev compose-up
- RTE=prod compose-up

### Import data and users to the project
- Open shell menu for the docker container with
  - RTE=dev docker-compose exec app sh
- Create a super user with the command
  - python bank_project/manage.py createsuperuser
- Import ranks with
  - python  bank_project/manage.py provision
- Optionally, import test customers and staff with existing loans and transfers with
  - python  bank_project/manage.py demodata



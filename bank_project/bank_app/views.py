from decimal import Decimal
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.db import IntegrityError
from django.conf import settings
from .forms import TransferForm, UserForm, CustomerForm, NewUserForm, NewAccountForm, LoanForm, ExternalTransferForm
from .models import Account, Ledger, Customer, Bank, LoanRequest
from .errors import InsufficientFunds
from django_otp.decorators import otp_required
import requests


# View redirects users to appropiate dashboard based on their is_staff status in the user model
@login_required
def index(request):
    if request.user.is_staff:
        return HttpResponseRedirect(reverse('bank_app:staff_dashboard'))
    else:
        return HttpResponseRedirect(reverse('bank_app:dashboard'))


# View that assures user is not staff, then returns dashboard. View for all user accounts.
@login_required
def dashboard(request):
    assert not request.user.is_staff, 'Staff user trying to access customer page.'
    loan_requests = LoanRequest.objects.filter(user=request.user)
    accounts = request.user.customer.accounts
    context = {
        'accounts': accounts,
        'loan_requests': loan_requests,
    }

    return render(request, 'bank_app/dashboard.html', context)


# View checks if user is a customer, creates the account on POST and renders the form on a GET request
@login_required
def create_account(request):
    assert not request.user.is_staff, 'Staff user trying to access customer page.'
    if request.method == 'POST':
        new_account_form = NewAccountForm(request.POST)
        if new_account_form.is_valid():
            name = new_account_form.cleaned_data['name']
            Account.objects.create(user=request.user, name=name)
            return HttpResponseRedirect(reverse('bank_app:dashboard'))
    else:
        new_account_form = NewAccountForm()

    context = {
        'new_account_form': new_account_form,
    }

    return render(request, 'bank_app/create_account.html', context)


# View that shows details/transactions of a users account, checks if the user owns it before showing
@login_required
def account_details(request, pk):
    account = get_object_or_404(Account, pk=pk)
    movements = Ledger.objects.filter(account=account)
    assert not request.user.is_staff, 'Staff user trying to access customer page.'
    if not request.user == account.user:
        raise PermissionDenied('Access Denied - Not your account.')

    context = {
        'movements': movements,
        'account': account,
        'interest_rate': settings.INTEREST_RATE
    }

    return render(request, 'bank_app/account_details.html', context)


# View that shows the details of a specific transfer once it has been sent
@login_required
def transaction_details(request, transaction):
    movements = Ledger.objects.filter(transaction=transaction)
    assert not request.user.is_staff, 'Staff user trying to access customer page.'
    if not movements.filter(account__in=request.user.customer.accounts):
        raise PermissionDenied('Customer is not a part of the transaction.')

    context = {
        'movements': movements,
    }

    return render(request, 'bank_app/transaction_details.html', context)


# View for transfering money between accounts, asserts that only users can transfer on their own behalf
@login_required
def transfer(request):
    assert not request.user.is_staff, 'Staff user trying to access the customer page.'

    if request.method == 'POST':
        form = TransferForm(request.POST)
        form.fields['debit_account'].queryset = request.user.customer.accounts
        if form.is_valid():
            amount = form.cleaned_data['amount']
            debit_account = Account.objects.get(pk=form.cleaned_data['debit_account'].pk)
            debit_text = form.cleaned_data['debit_text']
            credit_account = Account.objects.get(pk=form.cleaned_data['credit_account'])
            credit_text = form.cleaned_data['credit_text']
            try:
                transfer = Ledger.transfer(amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer.'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts
    extform = ExternalTransferForm()
    extform.fields['debit_account'].queryset = request.user.customer.accounts
    extform.fields['credit_bank'].queryset = Bank.objects.all()
    context = {
        'form': form,
        'extform': extform,
    }
    return render(request, 'bank_app/transfer.html', context)


@login_required
def external_transfer(request):
    assert not request.user.is_staff, 'Staff user trying to access the customer page.'

    if request.method == 'POST':
        extform = ExternalTransferForm(request.POST)
        extform.fields['debit_account'].queryset = request.user.customer.accounts
        extform.fields['credit_bank'].queryset = Bank.objects.all()

        if extform.is_valid():
            amount = extform.cleaned_data['amount']
            debit_account = Account.objects.get(pk=extform.cleaned_data['debit_account'].pk)
            debit_text = extform.cleaned_data['debit_text']
            credit_bank_address = extform.cleaned_data['credit_bank'].address
            credit_account = extform.cleaned_data['credit_account']
            credit_text = extform.cleaned_data['credit_text']

            # Get debit_bank_address from the current customer's bank
            debit_bank_address = request.user.customer.bank.address

            try:

                # Check if the transfer amount exceeds the available balance
                if amount > debit_account.balance:
                    raise InsufficientFunds  # Raise the InsufficientFunds exception
                # Prepare the transfer payload
                transfer_payload = {
                    'amount': str(amount),  # Convert to string
                    'debit_account': debit_account.pk,
                    'debit_text': debit_text,
                    'debit_bank_address': debit_bank_address,  # Include debit_bank_address
                    'credit_bank_address': credit_bank_address,
                    'credit_account': credit_account,  # Include credit_account
                    'credit_text': credit_text,
                }

                # Construct the API URL
                api_url = f'{debit_bank_address}bank/api/startExtTransfer/'

                # Send the POST request to the API endpoint
                requests.post(api_url, json=transfer_payload)

                # Transfer successful
                return account_details(request, debit_account.pk)

            except requests.exceptions.HTTPError as e:
                # Transfer failed
                context = {
                    'title': 'Transfer Error',
                    'error': str(e)
                }
                return render(request, 'bank_app/error.html', context)

            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer.'
                }
                return render(request, 'bank_app/error.html', context)

    else:
        extform = ExternalTransferForm()
        extform.fields['debit_account'].queryset = request.user.customer.accounts
        extform.fields['credit_bank'].queryset = Bank.objects.all()

    form = TransferForm()
    form.fields['debit_account'].queryset = request.user.customer.accounts

    context = {
        'form': form,
        'extform': extform,
    }

    return render(request, 'bank_app/transfer.html', context)


# View checks is user is approved for loan based on their rank
# If user is approved, on POST, a loan is created as an account with an extra is_loan = True variable.
# On GET request the loan page is rendered
@login_required
def loan(request):
    assert not request.user.is_staff, 'Staff user trying to access customer page.'

    if not request.user.customer.loan_approved:
        context = {
            'title': 'Create Loan Error',
            'error': 'You are not approved for a loan.'
        }

        return render(request, 'bank_app/error.html', context)

    if request.method == 'POST':
        form = LoanForm(request.POST)
        form.fields['credit_account'].queryset = request.user.customer.accounts

        request.user.customer.create_loan(
            Decimal(request.POST['amount']),
            request.POST['loan_description'],
            request.POST['credit_account']
        )

        # Redirect to the loan request details page or a confirmation page
        return HttpResponseRedirect(reverse('bank_app:dashboard'))
    else:
        form = LoanForm()

    form.fields['credit_account'].queryset = request.user.customer.accounts.filter(loan=False)
    context = {
        'form': form,
    }
    return render(request, 'bank_app/loan.html', context)


@login_required
def loan_requests_view(request):
    assert request.user.is_staff
    loan_requests = LoanRequest.objects.all()
    return render(request, 'bank_app/loan-requests.html', {'loan_requests': loan_requests})


@login_required
def approve_loan_request(request, loan_request_id):
    assert request.user.is_staff
    loan_request = LoanRequest.objects.get(pk=loan_request_id)
    loan_request.status = 'Approved'
    loan_request.save()

    # Run the Ledger function and create the loan
    target_account = loan_request.account
    loan = Account.objects.create(
        user=loan_request.user,
        name=f'LOAN: {loan_request.name}',
        loan=True
    )
    Ledger.transfer(
        loan_request.amount,
        loan,
        f'Load paid out to account {target_account.name}',
        target_account,
        f'Credit from loan {loan.pk}: {loan.name}',
        is_loan=True
    )

    # Redirect back to loan requests
    return redirect('bank_app:loan_requests')


@login_required
def decline_loan_request(request, loan_request_id):
    assert request.user.is_staff
    loan_request = LoanRequest.objects.get(pk=loan_request_id)
    loan_request.status = 'Declined'
    loan_request.save()

    # Redirect back to loan requests
    return redirect('bank_app:loan_requests')


# Staff dashboard view, asserts that user is actually staff
@login_required
def staff_dashboard(request):
    assert request.user.is_staff, 'Customer trying to access staff page.'
    return render(request, 'bank_app/staff_dashboard.html')


# Search bar for staff users that will display a customer list based on search_term
@login_required
def staff_search(request):
    assert request.user.is_staff, 'Customer trying to access staff page.'

    search_term = request.POST['search_term']
    customers = Customer.search(search_term)
    context = {
        'customers': customers,
    }
    return render(request, 'bank_app/staff_search.html', context)


# View checks is user is staff, then displays information about a customer and lets staff update the customer info
@login_required
def staff_customer_details(request, pk):
    assert request.user.is_staff, 'Customer trying to access staff page.'

    customer = get_object_or_404(Customer, pk=pk)
    if request.method == 'GET':
        user_form = UserForm(instance=customer.user)
        customer_form = CustomerForm(instance=customer)
    elif request.method == 'POST':
        user_form = UserForm(request.POST, instance=customer.user)
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()
    new_account_form = NewAccountForm()
    context = {
        'customer': customer,
        'user_form': user_form,
        'customer_form': customer_form,
        'new_account_form': new_account_form,
    }
    return render(request, 'bank_app/staff_customer_details.html', context)


# View checks is user is staff, then displays all customer accounts on their customer page
@login_required
def staff_account_list(request, pk):
    assert request.user.is_staff, 'Customer trying to access staff page.'

    customer = get_object_or_404(Customer, pk=pk)
    accounts = customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank_app/staff_account_list.html', context)


# View that gets staff a list of all movements/transfers the user has had on their account
# Requires staff to have 2FA to access for security reasons
@otp_required
@login_required
def staff_account_details(request, pk):
    account = get_object_or_404(Account, pk=pk)
    movements = Ledger.objects.filter(account=account)
    if not request.user.is_staff:
        if not movements.filter(account__in=request.user.customer.accounts):
            raise PermissionDenied('Customer is not a part of the transaction.')
    context = {
        'movements': movements,
        'account': account,
        'interest_rate': settings.INTEREST_RATE
    }
    return render(request, 'bank_app/staff_account_details.html', context)


# View that lets staff change the status of a specific account between active and inactive
@login_required
def staff_toggle_account_status(request, pk):
    assert request.user.is_staff, 'Customer trying to access staff page.'
    account = get_object_or_404(Account, pk=pk)
    if account.is_active:
        account.is_active = False
    else:
        account.is_active = True
    account.save()
    return HttpResponseRedirect(reverse('bank_app:staff_customer_details', args=(account.user.pk,)))


# Uses a form on the customer details page to create an account for the specific cutomer using their pk.
@login_required
def staff_create_account(request, user):
    assert request.user.is_staff, 'Customer trying to access staff page.'
    if request.method == 'POST':
        new_account_form = NewAccountForm(request.POST)
        if new_account_form.is_valid():
            Account.objects.create(user=User.objects.get(pk=user), name=new_account_form.cleaned_data['name'])
    return HttpResponseRedirect(reverse('bank_app:staff_customer_details', args=(user,)))


# View for staff members to create new users/customers in the system
@login_required
def staff_new_customer(request):
    assert request.user.is_staff, 'Customer trying to access staff page.'
    if request.method == 'POST':
        new_user_form = NewUserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        if new_user_form.is_valid() and customer_form.is_valid():
            username = new_user_form.cleaned_data['username']
            first_name = new_user_form.cleaned_data['first_name']
            last_name = new_user_form.cleaned_data['last_name']
            email = new_user_form.cleaned_data['email']
            password = new_user_form.cleaned_data['password']
            bank = Customer.objects.get(user_id=3).bank
            rank = customer_form.cleaned_data['rank']
            phone = customer_form.cleaned_data['phone']
            try:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                    email=email,
                    first_name=first_name,
                    last_name=last_name
                )
                print(f'********** Username: {username} -- Password: {password}')
                Customer.objects.create(user=user, rank=rank, phone=phone, bank=bank)
                return staff_customer_details(request, user.pk)
            except IntegrityError:
                context = {
                    'title': 'Database Error',
                    'error': 'User could not be created.'
                }
    else:
        new_user_form = NewUserForm()
        customer_form = CustomerForm()
    context = {
        'new_user_form': new_user_form,
        'customer_form': customer_form,
    }
    return render(request, 'bank_app/staff_new_customer.html', context)


# View that pulls all objects (transactions) from the ledger
# Displays results ordered by timestamp and pagination is added (10 per page)
@login_required
def staff_ledger(request):
    ledger = Ledger.objects.all().order_by("-timestamp")
    page = request.GET.get('page', 1)
    paginator = Paginator(ledger, 10)
    if not request.user.is_staff:
        raise PermissionDenied('Customer trying to access staff page.')
    try:
        ledger = paginator.page(page)
    except PageNotAnInteger:
        ledger = paginator.page(1)
    except EmptyPage:
        ledger = paginator.page(paginator.num_pages)
    context = {
        'ledger': ledger
    }
    return render(request, 'bank_app/staff_ledger.html', context)

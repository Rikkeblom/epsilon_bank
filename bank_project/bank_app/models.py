from decimal import Decimal
from rest_framework.response import Response
from django.db import models, transaction
from django.contrib.auth.models import User
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.db.models.query import QuerySet
from .errors import InsufficientFunds
from django.db.models import Q
import uuid
import json


# Bank Models under here
class Bank(models.Model):
    name = models.CharField(max_length=35, unique=True)
    address = models.URLField()

    def __str__(self):
        return f'{self.name} at {self.address}'


# Rank Models under here     
class Rank(models.Model):
    name = models.CharField(max_length=35, unique=True, db_index=True)
    value = models.IntegerField(unique=True, db_index=True)
    limit = models.DecimalField(decimal_places=2, max_digits=10)

    # Sets default rank to min database value
    @classmethod
    def default_rank(cls):
        return cls.objects.all().aggregate(models.Min('value'))['value__min']

    def __str__(self):
        return f'{self.value}:{self.name}'


# Customer Models under here                                  
class Customer(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    phone = models.CharField(max_length=25, db_index=True)
    rank = models.ForeignKey(Rank, default=1, on_delete=models.PROTECT)  # Default Bronze
    bank = models.ForeignKey(Bank, on_delete=models.PROTECT)

    # Model that takes first and last name of user and displays it as string.
    @property
    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    # Model that returns all Accounts, filtered by current user
    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    # Model that returns a true/false bool
    # Based on the user's rank.value comapred to a set limit in the settings.py file
    @property
    def loan_approved(self) -> bool:
        return self.rank.value >= settings.LOAN_APPROVAL_LIMIT

    def __str__(self):
        return f'{self.pk}: {self.full_name}'

    # Model that allows us to display results based on search_term (POST)
    @classmethod
    def search(cls, search_term):
        # stops search if length of search_term is 0.
        if len(search_term) >= 1:
            return cls.objects.filter(
                Q(user__id__contains=search_term) |
                Q(user__first_name__contains=search_term) |
                Q(user__last_name__contains=search_term) |
                Q(user__email__contains=search_term) |
                Q(phone__contains=search_term)
            )[:10]  # Limited to 10 results

    @property
    def main_account(self):
        return Account.objects.filter(user=self.user).first()

    # Model send a loan request
    def create_loan(self, amount, name, account_pk):
        account = Account.objects.get(pk=account_pk)
        assert account.is_active, 'Cannot create a loan for an inactive credit account.'
        assert self.loan_approved, 'Your customer rank does not allow you to take up loans.'
        assert amount > 0, 'You cannot make a loan for a non-positive amount.'
        assert amount <= self.user.customer.rank.limit, 'Your rank does not allow for that high a loan.'
        assert not account.loan, 'Cannot credit loan to a loans account.'

        loan_request = LoanRequest.objects.create(user=self.user, amount=amount, name=name, account_id=account_pk)

        return loan_request

    # Model approves a loan by creating an account for the user and making a Ledger transfer
    def approve_loan_request(self, loan_request_id):
        loan_request = LoanRequest.objects.get(pk=loan_request_id)
        loan_request.is_approved = True
        loan_request.save()

        # Run the Ledger function and create the loan
        target_account = loan_request.account
        loan = Account.objects.create(user=loan_request.user, name=f'LOAN: {loan_request.name}', loan=True)

        Ledger.transfer(
            loan_request.amount,
            loan,
            f'Loan paid out to account {target_account.name}',
            target_account,
            f'Credit from loan {loan.pk}: {loan.name}',
            is_loan=True
        )

        # Model declines a loan by deleting the loan_request.
        def decline_loan_request(self, loan_request_id):
            loan_request = LoanRequest.objects.get(pk=loan_request_id)
            loan_request.delete()


# Account Models under here                                   
class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=50, db_index=True)
    is_active = models.BooleanField(default=True)
    loan = models.BooleanField(default=False)

    class Meta:
        get_latest_by = 'pk'

    @property
    def movements(self) -> QuerySet:
        return Ledger.objects.filter(account=self)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    def loan_interest(self) -> Decimal:
        if self.balance < 0 and self.loan:
            return abs(Decimal((self.balance / 100) * settings.INTEREST_RATE))

    def add_loan_interest(self):
        assert self.loan, 'Account is not a loan'
        assert self.balance < 0, 'Loan is payed'
        debit_account = self
        amount = self.loan_interest()
        credit_account = Account.objects.get(name='Bank OPS Account')

        Ledger.transfer(
            amount,
            debit_account,
            'Interest on loan',
            credit_account,
            'Interest on loan',
            is_loan=True
        )

    def __str__(self):
        return f'{self.pk}. {self.name}: {self.balance}'


# Loan Models under here
class LoanRequest(models.Model):
    STATUS_CHOICES = (
        ('Pending', 'Pending'),
        ('Approved', 'Approved'),
        ('Declined', 'Declined'),
    )

    user = models.ForeignKey(User, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    name = models.CharField(max_length=100)
    account = models.ForeignKey(Account, on_delete=models.PROTECT)  # Replace 'yourapp' with your actual app name
    is_approved = models.BooleanField(default=False)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='Pending')


# Ledger Models under here                                    
class Ledger(models.Model):
    transaction = models.UUIDField(default=uuid.uuid4, editable=False)
    account = models.ForeignKey(Account, on_delete=models.PROTECT, null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    text = models.TextField()

    # This classmethod handles internal transfers from within the same bank
    @classmethod
    def transfer(cls, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False) -> int:
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                # Makes sure you can not transfer to an inactive account
                assert debit_account.is_active, 'Debit account must be active'
                assert credit_account.is_active, 'Credit account must be active'

                if credit_account.loan and amount + credit_account.balance > 0:
                    # Automatically caps loan payoff to loan amount
                    amount = -credit_account.balance

                # Uses uuid to create a id for the transaction
                uid = uuid.uuid4()
                cls(amount=-amount, transaction=uid, account=debit_account, text=debit_text).save()
                cls(amount=amount, transaction=uid, account=credit_account, text=credit_text).save()
                # Set the loan account to inactive when it reaches 0
                if credit_account.loan and credit_account.balance >= 0:
                    account = get_object_or_404(Account, pk=credit_account.pk)
                    account.is_active = False
                    account.save()
            else:
                raise InsufficientFunds
        return uid

    # This classmethod handles the transfers in the sender bank on external transactions using REST API
    @classmethod
    def transferout(cls, amount, debit_account, to_bank, debit_text, credit_account, credit_text, ext_uuid, is_loan=False) -> int:
        with transaction.atomic():

            debit_acc = Account.objects.get(pk=debit_account)

            if debit_acc.balance >= amount or is_loan:
                # Makes sure the debit account has enough funds
                assert debit_acc.is_active, 'Debit account must be active'

                # if credit_account.loan and amount + credit_account.balance > 0:
                # Automatically caps loan payoff to loan amount
                # amount = -credit_account.balance

                # Uses uuid to create a id for the transaction
                uid = ext_uuid

                # Log money going out as usual in the Ledger
                cls(amount=-amount, transaction=uid, account=debit_acc, text=debit_text).save()

                # Log money going into the external bank in the Ledger using empty account
                cls(amount=amount, transaction=uid, account=Account.objects.get(pk=1), text=f'External transfer to {to_bank} account: {credit_account}').save()

                # Set the loan account to inactive when it reaches 0
                # if credit_account.loan and credit_account.balance >= 0:
                # account = get_object_or_404(Account, pk=credit_account.pk)
                # account.is_active = False
                # account.save()
            else:
                raise InsufficientFunds
        return uid

    # This classmethod handles the transfers in the receiver bank on external transactions using REST API
    @classmethod
    def transferin(cls, amount, debit_account, from_bank, debit_text, credit_account, credit_text, ext_uuid, is_loan=False) -> int:
        with transaction.atomic():
            credit_acc = Account.objects.get(pk=credit_account)

            # Create a unique transaction ID
            uid = ext_uuid

            # Log money coming into the account in the Ledger
            cls(amount=amount, transaction=uid, account=credit_acc, text=credit_text).save()

            # Log money coming from the external bank in the Ledger using empty account
            external_transfer_text = f'External transfer from {from_bank} account: {debit_account}'
            cls(amount=-amount, transaction=uid, account=Account.objects.get(pk=1), text=external_transfer_text).save()

        return uid

    def __str__(self):
        return f'{self.amount} :: {self.transaction} :: {self.timestamp} :: {self.account} :: {self.text}'


class Validation():

    # Transfer initial validation
    @classmethod
    def validate_payload(self, request):
        assert request.data.get('debit_bank_address'), 'Missing debit bank address.'
        assert request.data.get('credit_bank_address'), 'Missing credit bank address.'
        assert request.data.get('debit_account'), 'Missing debit account.'
        assert request.data.get('credit_account'), 'Missing credit account.'
        assert request.data.get('amount'), 'Missing amount'

        return Response({'Success': 'Payload validated succesfully'}, status=200)


class DataHandler():

    # Take parameter data -> json data
    @classmethod
    def to_json(self, request, ext_uuid):

        # Create payload object from request
        raw_data = {
            "debit_bank_address": request.data.get('debit_bank_address'),
            "credit_bank_address": request.data.get('credit_bank_address'),
            "debit_account": int(request.data.get('debit_account')),
            "credit_account": int(request.data.get('credit_account')),
            "amount": int(request.data.get('amount')),
            "debit_text": request.data.get('debit_text'),
            "credit_text": request.data.get('credit_text'),
            "transfer_id": str(ext_uuid)
        }
        return raw_data


class Status(models.Model):
    name = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return f'{self.name}'


class ExternalTransfers(models.Model):
    transaction = models.UUIDField(default=uuid.uuid4, editable=False)
    from_bank = models.CharField(max_length=50)
    to_bank = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    status = models.ForeignKey(Status, default=1, on_delete=models.PROTECT)  # default = 'Pending'

    # Gets transfer status by transfer id
    @classmethod
    def get_transfer_status(self, transfer_id):
        # Extract transfer id
        transfer_object = get_object_or_404(ExternalTransfers, transaction=transfer_id)
        transfer_status = transfer_object.status.name
        return transfer_status

    # Creates transfer in external transfers table and sets status to pending
    @classmethod
    def transfer_create_status(self, request):

        json_data = json.loads(request)

        # extract transfer information
        transfer_id = json_data.get('transfer_id')
        from_bank_address = json_data.get('debit_bank_address')
        to_bank_address = json_data.get('credit_bank_address')

        # create the external transfer with default status id value
        transfer_object = ExternalTransfers.objects.create(
            transaction=transfer_id,
            from_bank=from_bank_address,
            to_bank=to_bank_address
        )

        return Response({'Success': f'Created transfer with id: {transfer_object.transaction}'}, status=200)

    @classmethod
    def update_transfer_status(cls, transfer_id, new_status):
        # Get the transfer object by ID
        transfer_object = get_object_or_404(ExternalTransfers, transaction=transfer_id)

        # Update the status based on the provided new_status value
        if new_status == 'Pending':
            transfer_object.status_id = 1
        elif new_status == 'Progress':
            transfer_object.status_id = 2
        elif new_status == 'Success':
            transfer_object.status_id = 3
        elif new_status == 'Error':
            transfer_object.status_id = 4
        else:
            return Response({'Error': 'invalid new_status'}, status=400)

        transfer_object.save()
        return Response({'Success': f'Updated status to: {transfer_object.status}'}, status=200)

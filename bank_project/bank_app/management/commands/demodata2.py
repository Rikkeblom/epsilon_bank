import secrets
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, Ledger, Customer, Rank, Bank


class Command(BaseCommand):
    def handle(self, **options):
        print('Adding demo data ...')

        # Getting rank data
        c_rank_1 = Rank.objects.get(pk=1)
        c_rank_2 = Rank.objects.get(pk=2)
        c_rank_3 = Rank.objects.get(pk=3)
        c_rank_5 = Rank.objects.get(pk=5)

        # Getting bank data
        bank_2 = Bank.objects.get(pk=2)  # Rikke

        # Creating a user 'Staff' with is_staff = True for permissions
        staff_user = User.objects.create_user('staff', email='staff@epsilon.com', password='staffer123')
        staff_user.first_name = 'Staff'
        staff_user.last_name = 'Stafler'
        staff_user.is_staff = True
        staff_user.save()

        # Creating a bank account for the bank
        bank_app_user = User.objects.create_user('Epsilon', email='', password=secrets.token_urlsafe(64))
        bank_app_user.is_active = False
        bank_app_user.save()
        Account.objects.create(user=bank_app_user, name='External Transfer Account')
        ipo_account = Account.objects.create(user=bank_app_user, name='Bank IPO Account')
        ops_account = Account.objects.create(user=bank_app_user, name='Bank OPS Account')
        Ledger.transfer(
            10_000_000,
            ipo_account,
            'Operational Credit',
            ops_account,
            'Operational Credit',
            is_loan=True
        )

        # Creating a user Sophie
        sophie_user = User.objects.create_user('sophie', email='sophie@banks.com', password='sophie123')
        sophie_user.first_name = 'Sophie'
        sophie_user.last_name = 'Banks'
        sophie_user.save()
        sophie_customer = Customer.objects.create(user=sophie_user, phone='12345678', rank=c_rank_2, bank=bank_2)
        sophie_customer.save()
        sophie_account = Account.objects.create(user=sophie_user, name='Checking account')
        sophie_account.save()
        sophie_account_two = Account.objects.create(user=sophie_user, name='Savings')
        sophie_account_two.is_active = False
        sophie_account_two.save()
        Ledger.transfer(
            12_750,
            ops_account,
            'Payout to Sophie',
            sophie_account,
            'Payout from bank'
        )

        # Creating a user James
        james_user = User.objects.create_user('james', email='james@smith.com', password='james123')
        james_user.first_name = 'James'
        james_user.last_name = 'Smith'
        james_user.save()
        james_customer = Customer.objects.create(user=james_user, phone='32521414', rank=c_rank_1, bank=bank_2)
        james_customer.save()
        james_account = Account.objects.create(user=james_user, name='Checking account')
        james_account.save()
        # Creating a transfer of 1500 from bank to James
        Ledger.transfer(
            1_500,
            ops_account,
            'Payout to James',
            james_account,
            'Payout from bank'
        )

        # Creating a user John
        john_user = User.objects.create_user('john', email='john@wick.com', password='john123')
        john_user.first_name = 'John'
        john_user.last_name = 'Wick'
        john_user.save()
        john_customer = Customer.objects.create(user=john_user, phone='32666666', rank=c_rank_3, bank=bank_2)
        john_customer.save()
        john_account = Account.objects.create(user=john_user, name='Payments')
        john_account.save()
        john_account_two = Account.objects.create(user=john_user, name='Old Account')
        john_account_two.is_active = False
        john_account_two.save()
        # Creating a transfer of 147500 from bank to John
        Ledger.transfer(
            109_500,
            ops_account,
            'Payout to Mr. Wick',
            john_account,
            'Transfer from bank.'
        )

        # Creating a user Viggo
        viggo_user = User.objects.create_user('viggo', email='viggo@tarasov.com', password='viggo123')
        viggo_user.first_name = 'Viggo'
        viggo_user.last_name = 'Tarasov'
        viggo_user.save()
        viggo_customer = Customer.objects.create(user=viggo_user, phone='22666999', rank=c_rank_5, bank=bank_2)
        viggo_customer.save()
        viggo_account = Account.objects.create(user=viggo_user, name='Main')
        viggo_account.save()
        # Creating a transfer of 147500 from bank to Viggo
        Ledger.transfer(
            8_547_500,
            ops_account,
            'Holdings.',
            viggo_account,
            'Deposit from holdings.'
        )

        # Creating some transfers between users
        Ledger.transfer(
            900,
            james_account,
            'Transfer to Sophie.',
            sophie_account,
            'Transfer from James.',
        )

        Ledger.transfer(
            425,
            sophie_account,
            'From Sophie - dinner.',
            james_account,
            'To James - dinner.',
        )

        Ledger.transfer(
            165_000,
            viggo_account,
            'For Mr. Wick.',
            john_account,
            'Always a pleasure.'
        )

        Ledger.transfer(
            250_000,
            viggo_account,
            'Baba Yaga.',
            john_account,
            'Job well done.'
        )

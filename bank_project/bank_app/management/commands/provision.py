from django.core.management.base import BaseCommand
from bank_app.models import Rank, Bank, Status


class Command(BaseCommand):
    def handle(self, **options):
        print('Provisioning ...')

        Bank.objects.create(name='Morten', address='http://194.195.243.124:8000/')
        Bank.objects.create(name='Rikke', address='http://139.144.178.193:8000/')
        Bank.objects.create(name='Felix', address='http://170.187.188.40:8000/')
        Bank.objects.create(name='Victoria', address='http://139.144.179.254:8000/')

        if not Rank.objects.all():
            Rank.objects.create(name='Bronze', value=20, limit=0)
            Rank.objects.create(name='Silver', value=40, limit=2000000)
            Rank.objects.create(name='Gold', value=60, limit=3000000)
            Rank.objects.create(name='Platinum', value=80, limit=4000000)
            Rank.objects.create(name='Diamond', value=100, limit=5000000)

        if not Status.objects.all():
            Status.objects.create(name='Pending')
            Status.objects.create(name='Progress')
            Status.objects.create(name='Success')
            Status.objects.create(name='Error')

from django.core.management.base import BaseCommand
from bank_app.models import Account


class Command(BaseCommand):
    def handle(self, **options):
        print("Adding interest rate on loans ...")

        # filter accounts to find all loans with a negative balance
        loanArray = Account.objects.filter(loan=True)
        for loan in loanArray:
            if loan.balance < 0 and loan.id != '1' and loan.id != '2' and loan.id != '3':
                # for each of these, call the apply interest method on the account
                loan.add_loan_interest()

# Generated by Django 4.2.1 on 2023-06-03 12:12

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('bank_app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=35, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='ExternalTransfers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transaction', models.UUIDField(default=uuid.uuid4, editable=False)),
                ('from_bank', models.CharField(max_length=50)),
                ('to_bank', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('status', models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, to='bank_app.status')),
            ],
        ),
    ]

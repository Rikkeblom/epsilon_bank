from django.test import TestCase
from django.urls import reverse
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from bank_app.models import Customer, Rank, Account, Ledger, Bank, Status


class TestsTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("--- setUpTestData: Run once to set up non-modified data for all class methods. ---")
        rank_1 = Rank.objects.create(name="Bronze", value=20, limit=0)
        rank_2 = Rank.objects.create(name="Silver", value=40, limit=2000000)
        bank_1 = Bank.objects.create(name="Morten", address="194.195.243.124")
        User.objects.create_user(
            "staff",
            first_name="Staff",
            last_name="Staffer",
            email="test@staff.com",
            password="staffTestPassword",
            is_staff=True,
        )
        bank_app_user = User.objects.create_user(
            "Epsilon",
            email="epsilon@bank.com",
            password="fake123",
            is_active=False
        )
        ipo_account = Account.objects.create(user=bank_app_user, name="Bank IPO Account")
        ops_account = Account.objects.create(user=bank_app_user, name="Bank OPS Account")
        james_user = User.objects.create_user(
            "james",
            first_name="James",
            last_name="Smith",
            email="test@james.com",
            password="jamesTestPassword",
            is_staff=False,
        )
        Customer.objects.create(
            user=james_user,
            phone="12345678",
            rank=rank_1,
            bank=bank_1
        )
        james_account_3 = Account.objects.create(
            user=james_user,
            name="James Test Account",
            is_active=True
        )
        james_account_4 = Account.objects.create(
            user=james_user,
            name="James Test Money Account"
        )
        james_account_5 = Account.objects.create(
            user=james_user,
            name="James Loan Account"
        )
        sophie_user = User.objects.create_user(
            "sophie",
            first_name="Sophie",
            last_name="Banks",
            email="test@sophie.com",
            password="sophieTestPassword",
            is_staff=False,
        )
        Customer.objects.create(
            user=sophie_user,
            phone="91011121",
            rank=rank_2,
            bank=bank_1
        )
        sophie_account_6 = Account.objects.create(
            user=sophie_user,
            name="Sophie Test Account"
        )
        sophie_account_7 = Account.objects.create(
            user=sophie_user,
            name="Sophie Test Inactive Account"
        )
        Ledger.transfer(
            10000000,
            ipo_account,
            'Operational Credit',
            ops_account,
            'Operational Credit',
            is_loan=True
        )
        Ledger.transfer(
            12000,
            ops_account,
            'Payout to Sophie',
            sophie_account_7,
            'Payout from bank'
        )
        Ledger.transfer(
            2000,
            sophie_account_7,
            'Transfer to first account',
            sophie_account_6,
            'Transfer from second account'
        )
        Ledger.transfer(
            10000,
            ops_account,
            'Payout to James',
            james_account_4,
            'Payout from bank'
        )
        Ledger.transfer(
            1000,
            james_account_4,
            'Transfer to first account',
            james_account_3,
            'Transfer from second account'
        )
        Ledger.transfer(
            1000,
            james_account_4,
            'Transfer to third account',
            james_account_5,
            'Transfer from second account'
        )
        Ledger.transfer(
            6000,
            sophie_account_7,
            'to James',
            james_account_4,
            'from Sophie'
        )

    def test_get_rank(self):
        rank_one = Rank.objects.get(id=1)
        rank_two = Rank.objects.get(id=2)
        assert rank_one.value < rank_two.value

    def test_user_can_not_take_loan(self):
        james = Customer.objects.get(user_id=3)
        with self.assertRaises(AssertionError):
            james.create_loan(1000, 'James Test Account', 1)

    def test_if_session_user_is_login_user(self):
        self.client.login(username='james', password='jamesTestPassword')
        response = self.client.get('/bank/dashboard/')
        self.assertEqual(str(response.context['user']), 'james')

    def test_user_can_take_loan(self):
        self.client.login(username='sophie', password='sophieTestPassword')
        response = self.client.post('/bank/loan/', {"loan_description": "test", "amount": 1000, "credit_account": 6})
        self.assertEqual(response.status_code, 302)

    def test_user_can_not_loan_more_than_rank_limit(self):
        sophie = Customer.objects.get(user_id=4)
        with self.assertRaises(AssertionError):
            sophie.create_loan(6000000, 'Sophie Test Account', 1)

    def test_user_can_not_loan_if_credit_account_is_inactive(self):
        sophie = Customer.objects.get(user_id=4)
        sophie_account = Account.objects.get(id=7)
        sophie_account.is_active = False
        sophie_account.save()  # Save the changes to the account

        with self.assertRaises(AssertionError):
            sophie.create_loan(1000, 'Sophie Test Account', sophie_account.pk)

    # def test_loan_become_inactive_after_pay(self):
        # self.client.login(username='sophie', password='sophieTestPassword')
        # sophie = Customer.objects.get(pk=4)
        # loan_request = sophie.create_loan(100, 'sophie loan test', 7)
        # sophies_account = Account.objects.get(pk=7)
        # sophie.approve_loan_request(1)
        # latest_loan = Account.objects.get(pk=8)
        # transfer = Ledger.transfer(100, sophies_account, 'paying back loan', latest_loan, 'from sophie paying back loan')
        # self.assertFalse(latest_loan.is_active)

    def test_user_can_make_transfer(self):
        self.client.login(username='james', password='jamesTestPassword')
        # james_account = Account.objects.get(id=4)
        # sophie_account = Account.objects.get(id=6)
        response = self.client.post('/bank/transfer/', {"amount": 1000, "debit_account": 4, "debit_text": "to sophie", "credit_account": 6, "credit_text": "from james"})
        self.assertEqual(response.status_code, 200)

    def test_user_can_not_transfer_if_credit_account_is_inactive(self):
        self.client.login(username='sophie', password='sophieTestPassword')
        sophie_account = Account.objects.get(id=7)
        sophie_account.is_active = False
        sophie_account.save()
        with self.assertRaises(AssertionError):
            self.client.post('/bank/transfer/', {"amount": 1000, "debit_account": 7, "debit_text": "to james", "credit_account": 4, "credit_text": "from sophie"})

    def test_user_can_not_transfer_if_debit_account_is_inactive(self):
        self.client.login(username='james', password='jamesTestPassword')
        james_account = Account.objects.get(id=4)
        james_account.is_active = False
        james_account.save()
        with self.assertRaises(AssertionError):
            self.client.post('/bank/transfer/', {"amount": 1000, "debit_account": 4, "debit_text": "to sophie", "credit_account": 7, "credit_text": "from james"})

    def test_user_can_logout(self):
        self.client.logout()
        response = self.client.get('/account/login/?next=/bank/')
        self.assertEqual(response.status_code, 200)

    def test_staff_can_make_new_customer(self):
        self.client.login(username='staff', password='staffTestPassword')
        response = self.client.post('/bank/staff_new_customer', {"username": "test", "first_name": "Tom", "last_name": "Thomasen", "email": "test@bank.com", "password": "tester123", "phone": 94650283})
        self.assertEqual(response.status_code, 200)

    def test_staff_can_make_account_inactive(self):
        self.client.login(username='staff', password='staffTestPassword')

        # Retrieve the user and account which is currently active
        james = User.objects.get(username='james')
        james_account = Account.objects.get(id=3)

        # Get the staff_customer_details page for the customer
        response = self.client.get(f"/bank/staff_customer_details/{james.customer.pk}/")
        self.assertEqual(response.status_code, 200)

        # Send request to url that toggles active status to opposite of current
        response = self.client.get(f"/bank/staff_toggle_account_status/{james_account.pk}/", follow=True)
        self.assertEqual(response.status_code, 200)

        # Retrieve the account from the database again to get the updated value
        updated_account = Account.objects.get(pk=james_account.pk)
        self.assertFalse(updated_account.is_active)

    def test_staff_can_update_all_input_on_user(self):
        self.client.login(username='staff', password='staffTestPassword')
        rank_two = Rank.objects.get(id=2)
        response = self.client.post('/bank/staff_customer_details/3/', {"first_name": "Jamesss", "last_name": "Smithhh", "email": "jamesss@bank.com", "rank": rank_two, "phone": 94650283})
        self.assertEqual(response.status_code, 200)

    def test_staff_can_make_make_account_for_user(self):
        self.client.login(username='staff', password='staffTestPassword')
        response = self.client.post('/bank/staff_customer_details/3/', {"name": "James account three"})
        self.assertEqual(response.status_code, 200)

    def test_user_redirected_to_correct_dashboard(self):
        # Log in as staff user
        self.client.login(username='staff', password='staffTestPassword')
        response = self.client.get(reverse('bank_app:index'))
        self.assertRedirects(response, reverse('bank_app:staff_dashboard'))
        self.client.logout()

        # Log in as a regular user
        self.client.login(username='james', password='jamesTestPassword')
        response = self.client.get(reverse('bank_app:index'))
        self.assertRedirects(response, reverse('bank_app:dashboard'))

    def test_create_account_view(self):
        self.client.login(username='james', password='jamesTestPassword')

        # Send a GET request to the create_account view
        response = self.client.get(reverse('bank_app:create_account'))
        self.assertEqual(response.status_code, 200)

        # Send a POST request to the create_account view with valid form data
        response = self.client.post(reverse('bank_app:create_account'), {'name': 'Test Account X'})
        self.assertRedirects(response, reverse('bank_app:dashboard'))

        # Check that the account is created in the database
        self.assertTrue(Account.objects.filter(user__username='james', name='Test Account X').exists())

    def test_transfer_zero_amount(self):
        sophie_account = Account.objects.get(pk=7)
        james_account = Account.objects.get(pk=4)
        with self.assertRaises(AssertionError):
            Ledger.transfer(0, sophie_account, 'amount:0', james_account, 'amount:0')

    def test_account_details_view(self):
        self.client.login(username='james', password='jamesTestPassword')
        # Trying to access James' account
        response = self.client.get(reverse('bank_app:account_details', args=(3,)))
        self.assertEqual(response.status_code, 200)

        # Try to access an account that doesn't belong to James (pk=2)
        response = self.client.get(reverse('bank_app:account_details', args=(2,)))
        self.assertEqual(response.status_code, 403)  # Expecting Forbidden response

        # Ensure PermissionDenied is raised
        with self.assertRaises(PermissionDenied):
            raise PermissionDenied("This should raise PermissionDenied")

    def test_staff_ledger_view(self):
        # Log in as staff user
        self.client.login(username='staff', password='staffTestPassword')
        response = self.client.get(reverse('bank_app:staff_ledger'))
        self.assertEqual(response.status_code, 200)
        self.client.logout()

        # Log in as a regular user
        self.client.login(username='james', password='jamesTestPassword')
        response = self.client.get(reverse('bank_app:staff_ledger'))
        self.assertEqual(response.status_code, 403)

    # Testing all the __str__ functions
    def test_customer_str(self):
        customer = Customer.objects.get(user__username='james')
        self.assertEqual(str(customer), "3: James Smith")

    def test_account_str(self):
        account = Account.objects.get(pk=3)
        self.assertEqual(str(account), "3. James Test Account: 1000.00")

    def test_rank_str(self):
        rank = Rank.objects.get(name="Bronze")
        self.assertEqual(str(rank), "20:Bronze")

    def test_status_str(self):
        # Create Status instance here
        status = Status(name="Pending")
        self.assertEqual(str(status), "Pending")

from rest_framework.response import Response
from rest_framework.decorators import api_view
from bank_app.models import Account, Ledger, DataHandler, ExternalTransfers
import uuid
import requests
import json


@api_view(['POST', 'PUT'])
def startExtTransfer(request):
    # Step 1 - Start the transfer protocol
    if request.method == "POST":
        ext_uuid = uuid.uuid4()  # UUID for transfer

        transfer_payload = json.dumps(DataHandler.to_json(request, ext_uuid))  # JSON payload from request
        json_data = json.loads(transfer_payload)

        # Reserve the transfer id, log it as pending in external_transfers table
        ExternalTransfers.transfer_create_status(transfer_payload)

        # Start link to other bank and send payload
        url_ip = json_data["credit_bank_address"]
        own_ip = json_data["debit_bank_address"]
        headers = {'content-type': 'application/json'}

        # Sending post request to Step 2
        api_call = requests.post(f"{url_ip}bank/api/recieveExtTransfer/", data=transfer_payload, headers=headers)

        # Step 3
        # Checking response from Step 2 - Starts transfer on 200 OK
        if api_call.status_code == 200:

            ExternalTransfers.get_transfer_status(ext_uuid)  # Check transfer status

            new_status = 'Progress'  # Set new status to Progress
            ExternalTransfers.update_transfer_status(ext_uuid, new_status)  # Set transaction status to in progress

            # Local transaction
            Ledger.transferout(json_data["amount"], json_data["debit_account"], json_data["credit_bank_address"], json_data["debit_text"], json_data["credit_account"], json_data['credit_text'], ext_uuid)

            # Send request to recieving bank to do the same and await response
            response = requests.put(f"{url_ip}bank/api/recieveExtTransfer/", data=transfer_payload, headers=headers)

            # Step 5 - Update to success on sender end, send put back to confirm.
            if response.status_code == 200:

                new_status = 'Success'  # Set new status to Success
                ExternalTransfers.update_transfer_status(ext_uuid, new_status)  # Set transaction status to in progress

                # Sending final put request to confirm success on sender end.
                final_request = requests.put(f"{url_ip}bank/api/recieveExtTransfer/", data=transfer_payload, headers=headers)

                if final_request.status_code == 200:
                    return Response({'Success': 'Transaction Complete!'}, status=200)
                else:
                    requests.put(f"{url_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
                    requests.put(f"{own_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
            else:
                requests.put(f"{url_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
                requests.put(f"{own_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
        else:
            requests.put(f"{url_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
            requests.put(f"{own_ip}bank/api/rollbackExtTransfer/", data=transfer_payload, headers=headers)
    # return Response({'Success': 'Called receiver with post successfully'}, status=200)


@api_view(['POST', 'PUT'])
def recieveExtTransfer(request):

    transfer_payload = json.dumps(request.data)
    json_data = json.loads(transfer_payload)

    # Step 2 - Initial transfer request (POST)
    if request.method == 'POST':

        # Log the transfer as pending and if success respond back status=200
        ExternalTransfers.transfer_create_status(transfer_payload)

        return Response({'Success': 'Receiving with post successfully'}, status=200)

    # Step 4
    if request.method == 'PUT':

        transfer_status = ExternalTransfers.get_transfer_status(json_data["transfer_id"])  # Checking transfer status

        if transfer_status == 'Pending':

            # Update transfer status to in progress
            new_status = 'Progress'
            ExternalTransfers.update_transfer_status(json_data["transfer_id"], new_status)

            # Local transaction
            Ledger.transferin(json_data["amount"], json_data["debit_account"], json_data["debit_bank_address"], json_data["debit_text"], json_data["credit_account"], json_data['credit_text'], json_data["transfer_id"])

            return Response({'Success': 'Receiving with PUT @ Pending successfully'}, status=200)

        if transfer_status == 'Progress':

            new_status = 'Success'  # Set new status to Success
            ExternalTransfers.update_transfer_status(json_data["transfer_id"], new_status)  # Set transaction status to in progress

            return Response({'Success': 'External transfer status set to Success.'}, status=200)


@api_view(['POST'])
def rollbackTransaction(request):

    assert request.data.get('transaction_id'), 'Missing Transaction ID'
    transaction_id = request.data.get('transaction_id')
    transfer_payload = json.dumps(request.data)
    json_data = json.loads(transfer_payload)

    # Updating the status on the external transfers table to error
    ExternalTransfers.update_transfer_status(json_data["transfer_id"], 'Error')

    # check ledger for transactions with same id
    ledger_transactions = Ledger.objects.filter(transaction=transaction_id)
    print(ledger_transactions)

    account1 = Account.objects.get(id=ledger_transactions[0].account_id)
    account2 = Account.objects.get(id=ledger_transactions[1].account_id)

    # create a transfer with the opposite amount
    try:
        Ledger.transfer(
            ledger_transactions[0].amount,
            account1,
            'Reversing external transfer due to error',
            account2,
            'Reversing external transfer due to error',
            is_loan=True
        )
        return Response({'success': 'Transaction rolled back'}, status=200)
    except TransactionManagementError:
        return Response({'Error': 'Rollback failed'}, status=400)

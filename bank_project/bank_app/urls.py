# file: bank_project/bank_app/urls.py

# from django.contrib import admin
from django.urls import path
from . import views, api

app_name = "bank_app"

urlpatterns = [
    path('', views.index, name='index'),
    path('account_details/<int:pk>/', views.account_details, name='account_details'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('create_account/', views.create_account, name='create_account'),
    path('transfer/', views.transfer, name='transfer'),
    path('external_transfer/', views.external_transfer, name='external_transfer'),
    path('loan/', views.loan, name='loan'),
    path('transaction_details/<int:transaction>/', views.transaction_details, name='transaction_details'),

    path('api/startExtTransfer/', api.startExtTransfer),
    path('api/recieveExtTransfer/', api.recieveExtTransfer),
    path('api/rollbackTransaction/', api.rollbackTransaction),

    path('loan-requests/', views.loan_requests_view, name='loan_requests'),
    path('loan-requests/approve/<int:loan_request_id>/', views.approve_loan_request, name='approve_loan'),
    path('loan-requests/decline/<int:loan_request_id>/', views.decline_loan_request, name='decline_loan'),

    path('staff_ledger', views.staff_ledger, name='staff_ledger'),
    path('staff_dashboard', views.staff_dashboard, name='staff_dashboard'),
    path('staff_search/', views.staff_search, name='staff_search'),
    path('staff_customer_details/<int:pk>/', views.staff_customer_details, name='staff_customer_details'),
    path('staff_account_list/<int:pk>/', views.staff_account_list, name='staff_account_list'),
    path('staff_account_details/<int:pk>/', views.staff_account_details, name='staff_account_details'),
    path('staff_new_customer', views.staff_new_customer, name="staff_new_customer"),
    path('staff_create_account/<int:user>/', views.staff_create_account, name='staff_create_account'),
    path('staff_toggle_account_status/<int:pk>/', views.staff_toggle_account_status, name='staff_toggle_account_status'),
]

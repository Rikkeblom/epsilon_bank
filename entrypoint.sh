#!/bin/sh

cd bank_project
python manage.py check
python manage.py makemigrations
python manage.py migrate
cd ..

case "$RTE" in
    dev )
        echo "*** Running in development mode ***"
        
        cd bank_project
        python manage.py runserver 0.0.0.0:8000
        cd ..
        
        ;;
    test )
        echo "*** Running in Test mode ***"
        
        cd bank_project
        coverage run --source="bank_app/" --omit="bank_app/management/commands/*,bank_app/api.py" manage.py test --verbosity 2
        coverage report -m --fail-under=50
        cd ..
        
        ;;
    prod )
        echo "*** Running in Production mode ***"
esac

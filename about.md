---
layout: default
title: About Epsilon Bank
---

Epsilon Bank is a project created by
- Felix Bryld
- Morten Holm Sørensen
- Rikke Blom Thøgersen
- Victoria Bührmann

as part of the final exam of the course 'Development Environments' at [Copenhagen School of Design and Technology](https://kea.dk).
You can see the official requirements for the course [here](https://katalog.kea.dk/course/9941601/2022-2023).

## Project Features
- Staff:
    - Can search through all exisiting customers based on their name, phone number, email, and ID
    - Can see customer profiles, including full account overviews
    - Can change customer details such as first name, last name, email address, rank and phone number
    - Can add new accounts to a customer
    - Can change the status of any customer account
    - Can see a full overview of transfers on any customer account (only if the staff member uses 2-factor-login)
    - Can create new users
    - Can see the full ledger of transactions in the bank
    - Can approve or decline loan requests
    - Can add and remove 2-factor-authentication from their own profile.
- Customers
    - Can see an overview of all owned accounts, and customer rank
    - Can see the details of any owned accounts including title, status, balance, and all linked ledger transactions
    - Can create new accounts
    - Can transfer money between accounts
        - Can be both between the user's own accounts and to other customer accounts
        - Can be both in the same bank or other running bank
        - Can only transfer if the account has the needed funds
    - Can take loans
        - Max loan amount is set based on customer rank
        - Loans are payed back by transferring the loaned amount to the loan account
        - Loans must be approved by a staff member before being created
        - Loan accounts can never have a positive balance
        - If a customer tries to pay more than what is left on the loan, only the remaining loan amount will be charged
        - Until payed, loans will gain interest
            - frequency of the interests can be changed in docker-compose.yml
            - the percentage interest rate can be changed in settings.py
        - Once payed, loans turn inactive
    - Can add and remove 2-factor-authentication from their own profile.
- Banks
    - Separate bank instances can be started on the IP addresses which will allow for bank to bank transfers
        - 194.195.243.124:8000 # Morten
            - should be used in combination with demodata
        - 139.144.178.193:8000 # Rikke
            - should be used in combination with demodata2
        - 170.187.188.40:8000  # Felix
            - should be used in combination with demodata3
        - 139.144.179.254:8000  # Victoria
            - should be used in combination with demodata4


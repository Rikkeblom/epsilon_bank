#!/bin/sh

printf "\nChecking for vulnerabilities:\n"
pip-audit
printf "\nChecking .py files:\n"
find . -name '*.py' -exec sh -c "pylama -- {} && echo {}" \;
printf "\nChecking .sh files:\n"
find . -name '*.sh' -exec sh -c "shellcheck -- {} && echo {}" \; 
printf "\nChecking .yml files:\n"
find . -name '*.yml' -exec sh -c "yamllint -- {} && echo {}" \;
